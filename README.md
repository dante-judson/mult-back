# Projeto Mult
## Dante Judson Gomes de Lima

### Softwares Necessários
* Docker (Guia de instalação: https://docs.docker.com/engine/install/)
* Docker compose (Guia de instalação: https://docs.docker.com/compose/install/)

### Observação importante
O repositório do projeto Frontend encontra-se disponivel [aqui](https://gitlab.com/dante-judson/mult-ui.git) para consulta do código, mas para instalação do software não é necessario clonar o repositório do front, a imagem do docker fará o donwload do código e configuração no momento que o docker compose iniciar o build.

### Instalação do software
 
* Abra uma janela do teminal
* Clone o codigo deste repositório <br>
`git clone https://gitlab.com/dante-judson/mult-back.git`
* Navege até a pasta do projeto: <br>
`cd mult-back`
* Com o docker compose instalado rode o comando abaixo: <br>
`docker-compose up` <br>
Note: Dependendo das configurações do SO esse comando pode solicitar permissão de administrador para rodar corretamente
* A instalação e setup de todo o ambiente deve começar espere até ver a mensagem abaixo: <br>
`frontend_1  | : Compiled successfully.`
* A aplicação estará acessivel no browser no endereço http://localhost:4200 



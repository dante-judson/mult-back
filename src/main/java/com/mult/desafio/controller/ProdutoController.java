package com.mult.desafio.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mult.desafio.business.ProdutoBusiness;
import com.mult.desafio.business.exception.ProdutoBusinessException;
import com.mult.desafio.business.exception.ResourceNotFoundException;
import com.mult.desafio.entity.Produto;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("produto")
public class ProdutoController {
	
	@Autowired
	private ProdutoBusiness produtoBusiness;

	@GetMapping
	public ResponseEntity<List<Produto>> listAll() {
		return ResponseEntity.ok(this.produtoBusiness.listAll());
	}
	
	@PostMapping
	public ResponseEntity<Produto> create(@RequestBody @Valid Produto produto) {
		
		return ResponseEntity.status(HttpStatus.CREATED).body(this.produtoBusiness.create(produto));
	}
	
	@PutMapping
	public ResponseEntity<Produto> update(@RequestBody @Valid Produto produto) throws ResourceNotFoundException, ProdutoBusinessException {
		return ResponseEntity.ok(this.produtoBusiness.update(produto));
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Produto> findById(@PathVariable(name = "id") Long id) throws ResourceNotFoundException {
		return ResponseEntity.ok(this.produtoBusiness.findById(id));
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Produto> delete(@PathVariable(name = "id") Long id) throws ResourceNotFoundException {
		this.produtoBusiness.delete(id);
		return ResponseEntity.noContent().build();
	}
	
}

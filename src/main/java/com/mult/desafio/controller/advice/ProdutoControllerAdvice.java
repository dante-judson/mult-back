package com.mult.desafio.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.mult.desafio.business.exception.ProdutoBusinessException;
import com.mult.desafio.business.exception.ResourceNotFoundException;
import com.mult.desafio.entity.Categoria;

@ControllerAdvice
public class ProdutoControllerAdvice {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorDTO> handleValidationFailed(MethodArgumentNotValidException ex) {
		return ResponseEntity.badRequest().body(new ErrorDTO(ex.getFieldError().getDefaultMessage(), ex.getLocalizedMessage()));
	}
	
	@ExceptionHandler(InvalidFormatException.class)
	public ResponseEntity<ErrorDTO> handleInvalidFormatException(InvalidFormatException ex) {
		if (ex.getTargetType().equals(Categoria.class)) {			
			return ResponseEntity.badRequest().body(new ErrorDTO("Valor inválido para categoria, esperado: P - Perecível, NP - Não Perecível", ex.getLocalizedMessage()));
		}
		return ResponseEntity.badRequest().body(new ErrorDTO(ex.getMessage(), ex.getLocalizedMessage()));
	}
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<ErrorDTO> handleResourceNotFoundException(ResourceNotFoundException ex) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorDTO(ex.getMessage(), ex.getLocalizedMessage()));
	}
	
	@ExceptionHandler(ProdutoBusinessException.class)
	public ResponseEntity<ErrorDTO> handleProdutoBusinessException(ProdutoBusinessException ex) {
		return ResponseEntity.badRequest().body(new ErrorDTO(ex.getMessage(), ex.getLocalizedMessage()));
	}
	
	private class ErrorDTO {
		private String message;
		
		private String exception;

		public ErrorDTO(String message, String exception) {
			this.message = message;
			this.exception = exception;
		}
		
		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getException() {
			return exception;
		}

		public void setException(String exception) {
			this.exception = exception;
		}
		
		
	}
	
}

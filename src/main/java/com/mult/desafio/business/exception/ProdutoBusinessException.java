package com.mult.desafio.business.exception;

public class ProdutoBusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3390089859420418191L;

	public ProdutoBusinessException(String msg) {
		super(msg);
	}
}

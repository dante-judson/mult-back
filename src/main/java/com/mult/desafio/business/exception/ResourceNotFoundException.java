package com.mult.desafio.business.exception;

public class ResourceNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4584755509515151381L;
	
	public ResourceNotFoundException(String msg) {
		super(msg);
	}

}

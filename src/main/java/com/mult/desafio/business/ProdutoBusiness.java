package com.mult.desafio.business;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mult.desafio.business.exception.ProdutoBusinessException;
import com.mult.desafio.business.exception.ResourceNotFoundException;
import com.mult.desafio.entity.Produto;
import com.mult.desafio.repository.ProdutoRepository;

@Service
public class ProdutoBusiness {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	public List<Produto> listAll() {
		return this.produtoRepository.findAll();
	}
	
	public Produto create(Produto produto) {
		return this.produtoRepository.save(produto);
	}
	
	public void delete(Long id) throws ResourceNotFoundException {
		Produto toBeDeleted = this.findById(id);
		this.produtoRepository.delete(toBeDeleted);
	}
	
	public Produto findById(Long id) throws ResourceNotFoundException {
		Optional<Produto> op = this.produtoRepository.findById(id);
		op.orElseThrow(() -> new ResourceNotFoundException("Produto não encontrado"));
		return op.get();
	}
	
	public Produto update(Produto produto) throws ResourceNotFoundException, ProdutoBusinessException {
		if (produto.getId() == null) {
			throw new ProdutoBusinessException("Não pode atualizar produto sem ID");
		}
		Produto toBeUpdated = this.findById(produto.getId());
		if (!toBeUpdated.getNome().equals(produto.getNome())) {
			throw new ProdutoBusinessException("Campo NOME não pode ser alterado");
		}
		this.produtoRepository.save(produto);
		return produto;
	}
}

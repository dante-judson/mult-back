package com.mult.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mult.desafio.entity.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long>{

}

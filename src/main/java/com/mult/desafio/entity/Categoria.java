package com.mult.desafio.entity;

public enum Categoria {
	P("P","Pericível"),
	NP("NP","Não Perecível");
	
	private String diplayValue;
	private String value;
	
	private Categoria(String value, String displayValue) {
		this.value = value;
		this.diplayValue = displayValue;
	}
	
	public String getDisplayValue() {
		return this.diplayValue;
	}
	
	
}

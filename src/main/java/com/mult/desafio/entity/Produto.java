package com.mult.desafio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Entity
public class Produto {

	@SequenceGenerator(name="produto_id_seq",
            sequenceName="produto_id_seq",
            allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE
			,generator="produto_id_seq")
	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "NOME", updatable = false)
	@NotEmpty(message = "O campo NOME é obrigatório")
	private String nome;
	
	@Column(name = "VALOR")
	@NotNull(message = "O campo VALOR é obrigatório")
	@DecimalMin(value = "0.01", message = "O campo VALOR não pode ser 0")
	@DecimalMax(value = "9999999999999.99", message = "O campo VALOR acima do máximo 0"
			+ "permitido")
	private Float valor;
	
	@Column(name = "CATEGORIA")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "O campo CATEGORIA é obrigatório")
	private Categoria categoria;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public String getCategoria() {
		return categoria.getDisplayValue();
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
	
}
